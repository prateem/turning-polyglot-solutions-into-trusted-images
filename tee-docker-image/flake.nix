{
  description = "TEE workload docker image.";
  nixConfig.bash-prompt = "\[nix develop - tee\]$ ";
  inputs = {
    nixpkgs.url = git+https://github.com/nixos/nixpkgs.git?ref=nixpkgs-unstable;
    flake-utils.url = git+https://github.com/numtide/flake-utils.git?ref=main;
  };
  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system};
      in with pkgs; {
        packages.parquet_fdw = stdenv.mkDerivation {
          name = "parquet_fdw";
          makeFlags = "PG_CONFIG=${postgresql_14}/bin/pg_config";
          nativeBuildInputs = [ postgresql_14 arrow-cpp ];
          installPhase = ''
            install -D parquet_fdw.so -t $out/lib
            install -D parquet_fdw--0.1.sql "$out/share/postgresql/extension/parquet_fdw--0.1.sql"
            install -D parquet_fdw--0.1--0.2.sql "$out/share/postgresql/extension/parquet_fdw--0.1--0.2.sql"
            install -D parquet_fdw.control -t $out/share/postgresql/extension
          '';
          src = fetchGit {
            url = "https://github.com/adjust/parquet_fdw.git";
            rev = "3ee46aa78774159eb275a7734e3bfbfb95a9b573";
          };
        };
        packages.base = dockerTools.buildImage {
          name = "base";
          tag = "latest";
          copyToRoot = with dockerTools; [
            usrBinEnv
            # binSh
            bashInteractive
            caCertificates
            # fakeNss
          ];
        };
        packages.postgres = dockerTools.buildImage {
          name = "postgres";
          tag = "latest";
          fromImage = self.packages.${system}.base;
          copyToRoot = buildEnv {
            name = "image-root";
            paths = [ (postgresql_14.withPackages(_: [ self.packages.${system}.parquet_fdw ])) ];
            pathsToLink = [ "/bin" "/share" "/lib" ];
          };
          # runAsRoot = ''
          #   groupadd -r postgres --gid=999
          #   useradd -r -g postgres --uid=999 --home-dir=/var/lib/postgresql --shell=/bin/bash postgres
          #   mkdir -p /var/lib/postgresql
          #   chown -R postgres:postgres /var/lib/postgresql
          #   mkdir -p /var/run/postgresql && chown -R postgres:postgres /var/run/postgresql && chmod 2777 /var/run/postgresql
          #   mkdir -p /var/lib/postgresql/data && chown -R postgres:postgres /var/lib/postgresql/data && chmod 777 /var/lib/postgresql/data
          # '';
          config = {
            Env = [ "PGDATA=/var/lib/postgresql/data" "LANG=en_US.utf8" "LC_ALL=en_US.utf8" ];
          };
        };
        packages.dockerImage = dockerTools.buildImage {
          name = "us-central1-docker.pkg.dev/tee-project-0b43/tee-workloads/tee-nix-workload";
          tag = "debug";
          fromImage = self.packages.${system}.postgres;
          copyToRoot = buildEnv {
            name = "main-image";
            paths = [
              (buildEnv {
                name = "image-root";
                paths = [
                  ./scripts
                  google-cloud-sdk
                  shadow
                  busybox
                  htop
                  sudo
                ];
              })
            ];
          };
          config = {
            Entrypoint = [ "/bin/bash" "workload-entrypoint.sh" ];
            Env = [ "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin" ];
          };
        };
      });
}
