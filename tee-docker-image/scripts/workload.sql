create extension if not exists parquet_fdw;
create server if not exists parquet_srv foreign data wrapper parquet_fdw;

create function list_parquet_files(args jsonb)
returns text[] as
$$
begin
    return array_agg(args->>'dir' || '/' || filename)
           from pg_ls_dir(args->>'dir') as files(filename)
           where filename ~~ '%.parquet';
end
$$
language plpgsql;

drop foreign table dataprovider0_feature_stack;
drop foreign table dataprovider1_feature_stack;

select import_parquet('dataprovider0_feature_stack','public', 'parquet_srv', 'list_parquet_files', '{"dir":"/tmp/dataprovider0/input/dataprovider0_feature_stack_sample"}');
select import_parquet('dataprovider1_feature_stack','public', 'parquet_srv', 'list_parquet_files', '{"dir":"/tmp/dataprovider1/input/dataprovider1_feature_stack_sample"}');

DROP TABLE IF EXISTS dataprovider1_feature_stack_truncated;
CREATE TABLE dataprovider1_feature_stack_truncated AS
SELECT * FROM dataprovider1_feature_stack LIMIT 1000;

DROP TABLE IF EXISTS dataprovider1_feature_stack_truncated;
CREATE TABLE dataprovider1_feature_stack_truncated AS
SELECT * FROM dataprovider1_feature_stack LIMIT 1000;

-- Typically the actual business logic will include some kind of computation involving granular data
-- from multiple data providers which they do not want to disclose to each other that finally
-- results in some aggregate level result that is okay to share across all data providers.
DROP TABLE IF EXISTS combined_result;
CREATE TABLE combined_result
AS
SELECT
  sum(t0.revenue + t1.revenue) as total_revenue
FROM
  dataprovider0_feature_stack_truncated t0 JOIN
  dataprovider1_feature_stack_truncated t1
ON
  (t0.userid=t1.userid) AND
  (t0.act_time=t1.act_time) AND
  (t0.activity_type=t1.activity_type) AND
  (t0.actuuid = t1.actuuid);

-- Note it is possible to provide differential results to data providers based on some kind of
-- pre-agreement. This workload logic will be accessible and verifiable by all data providers
-- through which they can ensure that the agreement on which data providers collaborate and what
-- aggregate results get shared with which data providers. Typically this will also include some
-- audit results shared with all (or some) data providers for further validation.
copy combined_result to '/tmp/dataprovider0/output/dataprovider0_combined_result.csv';

copy combined_result to '/tmp/dataprovider1/output/dataprovider1_combined_result.csv';
