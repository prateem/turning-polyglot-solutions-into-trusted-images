#!/bin/bash

################################################################
# Definition of all constants and values read in from configs. #
################################################################
# These are the workload script's constants.
# OUTPUT_FOLDER="/tmp/postgres/archive/output"
SCRATCH_FOLDER_ROOT="/tmp"
SCRATCH_FOLDER_DATAPROVIDER1="/dataprovider1"
SCRATCH_FOLDER_DATAPROVIDER0="/dataprovider0"
SCRATCH_FOLDER_COMMON="/common"
STATUS_LOGFILE="/status.log"
ENTRYPOINT_ARGUMENTS_LOGFILE="/arguments.txt"
# derived constants
OUTPUT_FOLDER_LOG="${SCRATCH_FOLDER_ROOT}${SCRATCH_FOLDER_COMMON}"
OUTPUT_FOLDER="${OUTPUT_FOLDER_LOG}"
INPUT_FOLDER_DATAPROVIDER1="${SCRATCH_FOLDER_ROOT}${SCRATCH_FOLDER_DATAPROVIDER1}/input"
INPUT_FOLDER_DATAPROVIDER0="${SCRATCH_FOLDER_ROOT}${SCRATCH_FOLDER_DATAPROVIDER0}/input"
OUTPUT_FOLDER_DATAPROVIDER1="${SCRATCH_FOLDER_ROOT}${SCRATCH_FOLDER_DATAPROVIDER1}/output"
OUTPUT_FOLDER_DATAPROVIDER0="${SCRATCH_FOLDER_ROOT}${SCRATCH_FOLDER_DATAPROVIDER0}/output"
# NB: For following two values, it is not enough to change it here. These
# values actually reflect the default values in configuration 
PG_HOME="/var/lib/postgresql"
PG_RUN="/run/postgresql"

# These are config values. In production, these will be read from config datasource.
INPUT_BUCKET_DATAPROVIDER1="gs://tee-pub-dataprovider1-input"
OUTPUT_BUCKET_DATAPROVIDER1="gs://tee-pub-dataprovider1-output"
INPUT_BUCKET_DATAPROVIDER0="gs://tee-pub-dataprovider0-input"
OUTPUT_BUCKET_DATAPROVIDER0="gs://tee-pub-dataprovider0-output"
PROJECTID_DATAPROVIDER1="tee-project-0b43"
PROJECTID_DATAPROVIDER0="tee-project-0b43"
REGION_DATAPROVIDER1="us-central1"
REGION_DATAPROVIDER0="us-central1"
KEYRING_DATAPROVIDER1="tee-pub-dataprovider1-keyring"
KEYRING_DATAPROVIDER0="tee-pub-dataprovider0-keyring"
KEY_DATAPROVIDER1="tee-pub-dataprovider1-key"
KEY_DATAPROVIDER0="tee-pub-dataprovider0-key"
# The following should probably be workload script's constants rather than
# configs though they are metadata related to publishers, that is because
# if there is a change to this, probably it should get approved from all
# publishers as they relate to what data is published by which publisher
# and likely they are treated specially in the workload SQL anyway so no
# point abstracting it into configs.
declare -A PUBLISHER_OUTPUT_LOG_BUCKET=(["dataprovider1"]="${OUTPUT_BUCKET_DATAPROVIDER1}" ["dataprovider0"]="${OUTPUT_BUCKET_DATAPROVIDER0}")
declare -A INPUT_OBJECTS_DATAPROVIDER1=(
    ["/dataprovider1_event_stack_sample"]="/SimulationParquet/dataprovider1_event_stack_sample"
    ["/dataprovider1_feature_stack_sample"]="/SimulationParquet/dataprovider1_feature_stack_sample")
declare -A INPUT_OBJECTS_DATAPROVIDER0=(
    ["/dataprovider0_event_stack_sample"]="/SimulationParquet/dataprovider0_event_stack_sample"
    ["/dataprovider0_feature_stack_sample"]="/SimulationParquet/dataprovider0_feature_stack_sample")

########################################################################
# Cleanup/log copy operations. Set this up before doing anything else. #
########################################################################
# Function to save state before exiting.
# NB: Currently if there is an error during cleanup, the error message is sent to stderr
function cleanup {
    # Save the status log into publishers output buckets
    for publisher in "${!PUBLISHER_OUTPUT_LOG_BUCKET[@]}"; do
	# First remove any pre-existing status log file. Best effort, don't care if it fails.
	gcloud storage rm "${PUBLISHER_OUTPUT_LOG_BUCKET[${publisher}]}${STATUS_LOGFILE}"
	# Save the status log file of the current run to each publisher's output bucket.
	if ! gcloud storage cp "${OUTPUT_FOLDER}${STATUS_LOGFILE}" "${PUBLISHER_OUTPUT_LOG_BUCKET[${publisher}]}" ; then
	    printf "ERROR: Failed saving logs for publisher %s into bucket %s!\n" "${publisher}" "${PUBLISHER_OUTPUT_LOG_BUCKET[${publisher}]}" 1>&2
	else
	    printf "Saving logs for publisher %s into bucket %s.\n" "${publisher}" "${PUBLISHER_OUTPUT_LOG_BUCKET[${publisher}]}"
	fi
    done
    # Repeat the above pattern to save other log info if needed in future.
    return 0
}

trap cleanup SIGINT SIGTERM EXIT

###########################################################################
# Setup steps to bring container to a state where Pg dbms can be started. #
###########################################################################
# echo "Workload started 2." >> /tmp/postgres/archive/output/status.log

echo "$PATH"
echo "$PGDATA"
mkdir -p "${INPUT_FOLDER_DATAPROVIDER0}"  && chmod 0777 "${INPUT_FOLDER_DATAPROVIDER0}"
mkdir -p "${INPUT_FOLDER_DATAPROVIDER1}" && chmod 0777 "${INPUT_FOLDER_DATAPROVIDER1}"
mkdir -p "${OUTPUT_FOLDER_DATAPROVIDER0}" && chmod 0777 "${OUTPUT_FOLDER_DATAPROVIDER0}"
mkdir -p "${OUTPUT_FOLDER_DATAPROVIDER1}" && chmod 0777 "${OUTPUT_FOLDER_DATAPROVIDER1}"
mkdir -p "${SCRATCH_FOLDER_ROOT}${SCRATCH_FOLDER_COMMON}"

echo "Workload started." >> "${OUTPUT_FOLDER}${STATUS_LOGFILE}"

# FIXME: All of these steps in this block should be done at build time of
# the image and not later dynamically at runtime of the container. Can't
# do it because I can't create nested VM on local (because it is a VMWare
# guest) or on remote vm on cloud because org policy disallows it.

# create 'postgres' user and group.
groupadd -r postgres --gid=999
useradd -r -g postgres --uid=999 --home-dir="${PG_HOME}" --shell=/bin/sh postgres
# create the folders for Pg database to operate in.
mkdir -p "${PG_HOME}"
chown -R postgres:postgres "${PG_HOME}"
# mkdir -p /var/run/postgresql && chown -R postgres:postgres /var/run/postgresql && chmod 2777 /var/run/postgresql
mkdir -p "${PG_RUN}" && chown -R postgres:postgres "${PG_RUN}" && chmod 2777 "${PG_RUN}"
# mkdir -p /run/postgresql && chmod 2777 /run/postgresql
mkdir -p "${PG_HOME}/data" && chown -R postgres:postgres "${PG_HOME}/data" && chmod 0777 "${PG_HOME}/data"
# Create temporary directory; might already be created by this time though.
# But retaining this step here in case the scratch folder location is changed.
# /tmp is needed for lots of programs to run.
mkdir -p /tmp
chmod 0777 /tmp

# FIXME: en_US.utf8 is not working not sure why. C.utf8 does not seem to
# create any problems though.
export LC_ALL=C.utf8
export LANG=C.utf8

####################################
# Copy and decrypt all input data. #
####################################

echo "$@" >> "${OUTPUT_FOLDER}${ENTRYPOINT_ARGUMENTS_LOGFILE}"

# Copy step
for obj in "${!INPUT_OBJECTS_DATAPROVIDER1[@]}"; do
    # Create the destination folder on local filesystem.
    if ! mkdir -p "${INPUT_FOLDER_DATAPROVIDER1}" ; then
	printf "ERROR: Failed to create the destination folder ${INPUT_FOLDER_DATAPROVIDER1} on local filesystem to hold copied input data!\n" >> "${OUTPUT_FOLDER}${STATUS_LOGFILE}"
	exit 1;
    else
	:
    fi
    if ! gsutil -m cp -r "${INPUT_BUCKET_DATAPROVIDER1}${INPUT_OBJECTS_DATAPROVIDER1[${obj}]}" "${INPUT_FOLDER_DATAPROVIDER1}" ; then
	printf "ERROR: Failed to copy input object '%s'!\n" "${INPUT_BUCKET_DATAPROVIDER1}${INPUT_OBJECTS_DATAPROVIDER1[${obj}]}" >> "${OUTPUT_FOLDER}${STATUS_LOGFILE}"
	exit 1;
    else
	:
    fi
done
for obj in "${!INPUT_OBJECTS_DATAPROVIDER0[@]}"; do
    # Create the destination folder on local filesystem.
    if ! mkdir -p "${INPUT_FOLDER_DATAPROVIDER0}" ; then
	printf "ERROR: Failed to create the destination folder ${INPUT_FOLDER_DATAPROVIDER0} on local filesystem to hold copied input data!\n" >> "${OUTPUT_FOLDER}${STATUS_LOGFILE}"
	exit 1;
    else
	:
    fi
    if ! gsutil -m cp -r "${INPUT_BUCKET_DATAPROVIDER0}${INPUT_OBJECTS_DATAPROVIDER0[${obj}]}" "${INPUT_FOLDER_DATAPROVIDER0}" ; then
	printf "ERROR: Failed to copy input object '%s'!\n" "${INPUT_BUCKET_DATAPROVIDER0}${INPUT_OBJECTS_DATAPROVIDER0[${obj}]}" >> "${OUTPUT_FOLDER}${STATUS_LOGFILE}"
	exit 1;
    else
	:
    fi
done
echo "Input data copied from publisher GS buckets." >> "${OUTPUT_FOLDER}${STATUS_LOGFILE}"

# # Decrypt step
# for obj in "${!INPUT_OBJECTS_DATAPROVIDER1[@]}"; do
#     echo "gcloud kms decrypt --ciphertext-file=${SCRATCH_FOLDER}/${obj} --plaintext-file=${SCRATCH_FOLDER}/${INPUT_OBJECTS_DATAPROVIDER1[${obj}]} --key=projects/${PROJECTID_DATAPROVIDER1}/locations/${REGION_DATAPROVIDER1}/keyRings/${KEYRING_DATAPROVIDER1}/cryptoKeys/${KEY_DATAPROVIDER1}" >> "${OUTPUT_FOLDER}${STATUS_LOGFILE}"
#     if ! gcloud kms decrypt --ciphertext-file="${SCRATCH_FOLDER}"/"${obj}" --plaintext-file="${SCRATCH_FOLDER}"/"${INPUT_OBJECTS_DATAPROVIDER1[${obj}]}" --key=projects/"${PROJECTID_DATAPROVIDER1}"/locations/"${REGION_DATAPROVIDER1}"/keyRings/"${KEYRING_DATAPROVIDER1}"/cryptoKeys/"${KEY_DATAPROVIDER1}" ; then
# 	printf "ERROR: Failed to decrypt input object '%s/%s'!" "${SCRATCH_FOLDER}" "${obj}" >> "${OUTPUT_FOLDER}${STATUS_LOGFILE}"
# 	exit 1;
#     else
# 	:
#     fi
# done
# for obj in "${!INPUT_OBJECTS_DATAPROVIDER0[@]}"; do
#     if ! gcloud kms decrypt --ciphertext-file="${SCRATCH_FOLDER}"/"${obj}" --plaintext-file="${SCRATCH_FOLDER}"/"${INPUT_OBJECTS_DATAPROVIDER0[${obj}]}" --key=projects/"${PROJECTID_DATAPROVIDER0}"/locations/"${REGION_DATAPROVIDER0}"/keyRings/"${KEYRING_DATAPROVIDER0}"/cryptoKeys/"${KEY_DATAPROVIDER0}" ; then
# 	printf "ERROR: Failed to decrypt input object '%s/%s'!" "${SCRATCH_FOLDER}" "${obj}" >> "${OUTPUT_FOLDER}${STATUS_LOGFILE}"
# 	exit 1;
#     else
# 	:
#     fi
# done
# echo "Input data decrypted." >> "${OUTPUT_FOLDER}${STATUS_LOGFILE}"

# start Pg database management system
su -c "initdb -D ${PGDATA}" - postgres
su -c "pg_ctl -D ${PGDATA} -l ${PGDATA}/logfile start" - postgres
# sleep for some time to give Pg dbms enough time to finish its startup.
sleep 5

###########################
# Run the business logic. #
###########################
echo "Business logic execution started." >> "${OUTPUT_FOLDER}${STATUS_LOGFILE}"
su -c "PGPASSWORD=passwd psql -h 127.0.0.1 -p 5432 -d postgres -U postgres -a -f /workload.sql" - postgres
echo "$?" >> "${OUTPUT_FOLDER}${STATUS_LOGFILE}"
echo "Business logic execution completed." >> "${OUTPUT_FOLDER}${STATUS_LOGFILE}"

# ##################################################
# # encrypt and copy results out to output buckets #
# ##################################################
# if ! gcloud kms encrypt --ciphertext-file="${SCRATCH_FOLDER}"/gsexample1_copy.csv.dataprovider0-encrypted --plaintext-file="${SCRATCH_FOLDER}"/gsexample1_copy.csv --key=projects/"${PROJECTID_DATAPROVIDER0}"/locations/"${REGION_DATAPROVIDER0}"/keyRings/"${KEYRING_DATAPROVIDER0}"/cryptoKeys/"${KEY_DATAPROVIDER0}" ; then
#     printf "ERROR: Failed to encrypt output object %s!\n" "${SCRATCH_FOLDER}/gsexample1_copy.csv" >> "${OUTPUT_FOLDER}${STATUS_LOGFILE}"
#     exit 1
# else
#     printf "Output object %s encrypted successfully.\n" "${SCRATCH_FOLDER}/gsexample1_copy.csv.dataprovider0-encrypted" >> "${OUTPUT_FOLDER}${STATUS_LOGFILE}"
# fi
# # Remove old output object, before copying the new output object
# if gcloud storage ls "${OUTPUT_BUCKET_DATAPROVIDER0}"/gsexample1_copy.csv.dataprovider0-encrypted ; then
#     if ! gcloud storage rm "${OUTPUT_BUCKET_DATAPROVIDER0}"/gsexample1_copy.csv.dataprovider0-encrypted ; then
# 	printf "ERROR: Output object %s already exists and could not be removed!\n" "gsexample1_copy.csv.dataprovider0-encrypted" >> "${OUTPUT_FOLDER}${STATUS_LOGFILE}"
# 	exit 1
#     else
# 	:
#     fi
# else
#     :
# fi
# # Copy the new output to gs bucket
# if ! gcloud storage cp "${SCRATCH_FOLDER}"/gsexample1_copy.csv.dataprovider0-encrypted "${OUTPUT_BUCKET_DATAPROVIDER0}"/gsexample1_copy.csv.dataprovider0-encrypted ; then
#     printf "ERROR: Failed to copy output object %s!\n" "${OUTPUT_BUCKET_DATAPROVIDER0}/gsexample1_copy.csv.dataprovider0-encrypted" >> "${OUTPUT_FOLDER}${STATUS_LOGFILE}"
#     exit 1
# else
#     :   
# fi
# echo "Results copied to output bucket." >> "${OUTPUT_FOLDER}${STATUS_LOGFILE}"

for f in "${OUTPUT_FOLDER_DATAPROVIDER0}"/*; do
    if ! gsutil -m cp -r "$f" "${OUTPUT_BUCKET_DATAPROVIDER0}"; then
	printf "ERROR: Failed to copy the result %s to %s!\n" "${f}" "${OUTPUT_BUCKET_DATAPROVIDER0}" >> "${OUTPUT_FOLDER}${STATUS_LOGFILE}"
    fi
done
for f in "${OUTPUT_FOLDER_DATAPROVIDER1}"/*; do
    if ! gsutil -m cp -r "$f" "${OUTPUT_BUCKET_DATAPROVIDER1}"; then
	printf "ERROR: Failed to copy the result %s to %s!\n" "${f}" "${OUTPUT_BUCKET_DATAPROVIDER1}" >> "${OUTPUT_FOLDER}${STATUS_LOGFILE}"
    fi
done

echo "Workload finished." >> "${OUTPUT_FOLDER}${STATUS_LOGFILE}"
# # Keep the instance alive for connecting and debugging - and if
# # you are doing that then call cleanup() explicitly. Remove from
# # final version.
# cleanup
# while true
# do
#     echo "waiting"
#     sleep 60
# done
