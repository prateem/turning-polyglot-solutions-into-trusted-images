#!/usr/bin/env sh

groupadd -r postgres --gid=999
useradd -r -g postgres --uid=999 --home-dir=/var/lib/postgresql --shell=/bin/sh postgres
mkdir -p /var/lib/postgresql
chown -R postgres:postgres /var/lib/postgresql
mkdir -p /var/run/postgresql && chown -R postgres:postgres /var/run/postgresql && chmod 2777 /var/run/postgresql
mkdir -p /var/lib/postgresql/data && chown -R postgres:postgres /var/lib/postgresql/data && chmod 777 /var/lib/postgresql/data

while true
do
    echo "Press [CTRL-C] to stop.."
    sleep 60
done
